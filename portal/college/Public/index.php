<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/config/db.php');
if(!isset($_SESSION))
    session_start();

if(!isset($_SESSION["stud_id"]) || !isset($_GET["kc"]) || !isset($_GET["dt"]))
{
    header("Location:login.php");
    exit;
}

if(!(date("Y-m-d") === $_SESSION['dt'])){
    header("Location:login.php");
    exit;
}

$query = "SELECT * FROM tbltermstatus ORDER BY fldterm DESC LIMIT 1;";
$results = $conn->query($query);

if($results->num_rows){
    while($r = $results->fetch_assoc()){
        define("TERM",$r['fldcurrentterm']);   
        define("PERIOD","3");   
    }
}

session_write_close();

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <title>La Salle University - College Portal</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="shortcut icon" type="image/x-icon" href="img/icon.png" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="css/uniform.css" />
        <link rel="stylesheet" href="css/select2.css" />
        <link rel="stylesheet" href="css/matrix-style.css" />
        <link rel="stylesheet" href="css/matrix-media.css" />
        <link rel="stylesheet" href="css/custom.css" />
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/jquery.gritter.css" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'> </head>

    <body>
        <!--Header-part-->
        <div id="header">
            <h1><a href="dashboard.html">Matrix Admin</a></h1> </div>
        <!--close-Header-part-->
        <!--top-Header-menu-->
        <div id="user-nav" class="navbar navbar-inverse">
            <ul class="nav">
                <li class=""><img src="img/alternative.jpg" width="30px" height="10px" id="profilepic" /><span id="profname" class="text"></span> </li>
                <li class=""><a title="" href="login.php"><i class="icon icon-share-alt"></i><span class="text"> Logout</span></a></li>
            </ul>
        </div>
        <!--close-top-Header-menu-->
        <!--start-top-serch-->
        <!--close-top-serch-->
        <!--sidebar-menu-->
        <div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
            <ul>
                <li class="active"><a href="#"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
                <li> <a href="javascript:goPage('forms.php')"><i class="icon icon-signal"></i> <span>Forms</span></a> </li>
            </ul>
        </div>
        <!--sidebar-menu-->
        <!--main-container-part-->
        <div id="content">
            <!--breadcrumbs-->
            <div id="content-header">
                <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
            </div>
            <!--End-breadcrumbs-->
            <!--Chart-box-->
            <div class="container-fluid">
                <div class="row-fluid">
                    <!-- Start grades -->
                    <div class="span12">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-hand-right"></i> </span>
                                <h5>Announcements</h5> </div>
                            <div class="widget-content" id="widget-notif">
                                <div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
                                    <h4 class="alert-heading">Application for Graduation</h4> Application for graduation is now available Please <a href="https://docs.google.com/forms/d/e/1FAIpQLSc08_ojtNU8pajFBwv7gOzGEjN5Dana2mvilAuLDC_r88ZaGw/viewform"> Click here! </a> </div>
                            </div>
                        </div>
                    </div>
                    <!-- end grades -->
                </div>
                <hr/>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="widget-box">
                            <div class="widget-title bg_ly " data-toggle="collapse" style="background-color:#2ecc71;" href="#collapseG0"><span class="icon"><i class="icon-chevron-down" style="color:white;"></i></span>
                                <h5 style="color:white;">Personal Information</h5> </div>
                            <div class="widget-content nopadding collapse" id="collapseG0">
                                <!--<div align="center" style="background-color:#95a5a6;">
                                <img src="" id="profilepic" alt="img/alternative.jpg" class="avatar img-circle" />
                            </div> -->
                                <div align="center" style="background-color:#ecf0f1;">
                                    <table id="profiletbl" class="table table-striped allcenter">
                                        <thead>
                                            <tr>
                                                <th> Info </th>
                                            </tr>
                                        </thead>
                                        <tbody> </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- start bill -->
                    <div class="span4">
                        <div class="widget-box">
                            <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2" style="background-color:#2980b9;"><span class="icon"><i class="icon-chevron-down" style="color:white;"></i></span>
                                <h5 style="color:white;">Bill</h5><img src="img/f.gif" alt="demo-image" id="billloading" /> </div>
                            <div class="widget-content nopadding collapse " id="collapseG2">
                                <div class="control-group" style="text-align:center;">
                                    <label class="control-label">Choose</label>
                                    <div class="controls">
                                        <input type="radio" name="quarter" value="1" checked="true"> Prelim
                                        <input type="radio" name="quarter" value="2"> Midterm
                                        <input type="radio" name="quarter" value="3"> Semi-Finals
                                        <input type="radio" name="quarter" value="4"> Finals </div>
                                    <center>
                                        <button class="btn btn-success" id="btnBill">Update</button>
                                    </center>

                                </div>
                                <table class="table table-bordered table-striped" id="tblbill">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Statement of Account</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Previous Account</td>
                                            <td style="text-align:right;">0</td>
                                        </tr>
                                        <tr>
                                            <td>Tuition Fee</td>
                                            <td style="text-align:right;">0</td>
                                        </tr>
                                        <tr>
                                            <td>Laboratory Fee</td>
                                            <td style="text-align:right;">0</td>
                                        </tr>
                                        <tr>
                                            <td>School Fee</td>
                                            <td style="text-align:right;">0</td>
                                        </tr>
                                        <tr>
                                            <td>Other Fee</td>
                                            <td style="text-align:right;">0</td>
                                        </tr>
                                        <tr>
                                            <td>Additional Charges</td>
                                            <td style="text-align:right;">0</td>
                                        </tr>
                                        <tr>
                                            <td>Additional Charges one time</td>
                                            <td style="text-align:right;">0</td>
                                        </tr>
                                        <tr>
                                            <td><b>Total Charges</b></td>
                                            <td style="text-align:right;">0</td>
                                        </tr>
                                        <tr>
                                            <td>Payments</td>
                                            <td style="text-align:right;">0</td>
                                        </tr>
                                        <tr>
                                            <td>Discount</td>
                                            <td style="text-align:right;">0</td>
                                        </tr>
                                        <tr>
                                            <td><b>PRELIM Amount Due</b></td>
                                            <td style="text-align:right; font-weight: bold;">0</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- end bill   -->
                    <!-- start soa -->
                    <div class="span4">
                        <div class="widget-box" id="widgetsoa">
                            <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseGa" style="background-color:#34495e;"><span class="icon"><i class="icon-chevron-down" style="color:white"></i></span>
                                <h5 style="color:white">Statement of Account</h5><img src="img/f.gif" alt="demo-image" id="soaloading" /> </div>
                            <div class="widget-content nopadding collapse " id="collapseGa">
                                <table class="table table-bordered table-striped" id="tblsoa">
                                    <tbody>
                                        <tr>
                                            <td><b>Previous Account</b></td>
                                            <td style="text-align:right;" id="pa">0</td>
                                        </tr>
                                        <tr id="tfs">
                                            <td><b>Tuition Fee</b></td>
                                            <td style="text-align:right;" id="tf">0</td>
                                        </tr>
                                        <tr id="lfs">
                                            <td><b>Laboratory Fee</b></td>
                                            <td style="text-align:right;" id="lf">0</td>
                                        </tr>
                                        <tr>
                                            <td><b>School Fee</b></td>
                                            <td style="text-align:right;" id="sf">0</td>
                                        </tr>
                                        <tr id="ofss">
                                            <td><b>Addiotional Fee</b></td>
                                            <td style="text-align:right;" id="of">0</td>
                                        </tr>
                                        <tr id="pays">
                                            <td><b>Payments</b></td>
                                            <td style="text-align:right;" id="pay">0</td>
                                        </tr>
                                        <tr id="diss">
                                            <td><b>Discount</b></td>
                                            <td style="text-align:right;" id="dis">0</td>
                                        </tr>
                                        <tr>
                                            <td><b>Balance</b></td>
                                            <td style="text-align:right;" id="bal">0</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- end soa   -->
                </div>
                <hr/>
                <div class="row-fluid">
                    <div class="span6">
                        <div class="widget-box" id="widgetclearance">
                            <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseGc" style="background-color:#8e44ad;"><span class="icon"><i class="icon-chevron-down" style="color:white"></i></span>
                                <h5 style="color:white"> <b>Clearance</b> </h5><img src="img/f.gif" alt="demo-image" id="clearanceloading" /> </div>
                            <div class="widget-content nopadding collapse " id="collapseGc">
                                <div class="fix_hgt1">
                                    <table class="table table-striped table-bordered" id="tblclearance">
                                        <thead>
                                            <tr>
                                                <th colspan="3" id="clearancesemester">1st Semester</th>
                                            </tr>
                                            <tr>
                                                <th>Office / Department</th>
                                                <th>Remarks</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody> </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span3">
                        <div class="widget-box">
                            <div class="widget-title bg_ly " data-toggle="collapse" href="#collapses1" style="background-color:#E67E22;"><span class="icon"><i class="icon-chevron-down" style="color:white"></i></span>
                                <h5 style="color:white">Credentials</h5> </div>
                            <div class="widget-content nopadding collapse" id="collapses1">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="card">
                                            <td class="taskDesc"><i class="icon-folder-open"></i>Card</td>
                                            <td class="taskStatus"><span class="pending"><i class='icon-remove-sign' style='font-size:25px;'></i></span></td>
                                        </tr>
                                        <tr id="hondismissal">
                                            <td class="taskDesc"><i class="icon-plus-sign"></i>Honorable Dismissal</td>
                                            <td class="taskStatus"><span class="pending"><i class='icon-remove-sign' style='font-size:25px;'></i></span></td>
                                        </tr>
                                        <tr id="goodmoral">
                                            <td class="taskDesc"><i class="icon-ok-sign"></i>Good Moral</td>
                                            <td class="taskStatus"><span class="pending"><i class='icon-remove-sign' style='font-size:25px;'></i></span></td>
                                        </tr>
                                        <tr id="nso">
                                            <td class="taskDesc"><i class="icon-certificate"></i>NSO Birth Certificate</td>
                                            <td class="taskStatus"><span class="pending"><i class='icon-remove-sign' style='font-size:25px;'></i></span></td>
                                        </tr>
                                        <tr id="form137">
                                            <td class="taskDesc"><i class="icon-briefcase"></i>Form 137A</td>
                                            <td class="taskStatus"><span class="pending"><i class='icon-remove-sign' style='font-size:25px;'></i></span></td>
                                        </tr>
                                        <tr id="tor">
                                            <td class="taskDesc"><i class="icon-envelope"></i>Transcript of Record</td>
                                            <td class="taskStatus"><span class="pending"><i class='icon-remove-sign' style='font-size:25px;'></i></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="span3">
                        <div class="widget-box">
                            <div class="widget-title bg_ly " data-toggle="collapse" href="#collapses2" style="background-color:#E08283;"><span class="icon"><i class="icon-chevron-down" style="color:white"></i></span>
                                <h5 style="color:white">Internet Time</h5> </div>
                            <div class="widget-content nopadding collapse" id="collapses2">
                                <table class="table table-striped table-bordered allcenter">
                                    <tbody>
                                        <tr>
                                            <td><b>LAN:</b></td>
                                        </tr>
                                        <tr>
                                            <td><span id="lan"></span></td>
                                        </tr>
                                        <tr>
                                            <td><b>WLAN:</b></tr>
                                        <tr>
                                            <td><span id="wlan"></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row-fluid">
                    <!-- Start grades -->
                    <div class="span12">
                        <div class="widget-box" id="widgetterm">
                            <div class="widget-title bg_ly " data-toggle="collapse" href="#collapseG3" style="background-color:#f1c40f;"><span class="icon"><i class="icon-chevron-down" style="color:white;"></i></span>
                                <h5 style="color:white;">Available Grades</h5> <img src="img/f.gif" alt="demo-image" id="clearancegrading" /> </div>
                            <div class="widget-content nopadding collapse " id="collapseG3">
                                <div class="form-group" style="margin: 15px;" align="center">
                                    <select class="form-control" style="width: 100%;" name="Term">
                                        <option value="0">Choose</option>
                                    </select>

                                    <center>
                                        <button class="btn btn-success" id="btnGrades">Update</button>
                                    </center>
                                </div>

                                <div class="fix_hgt1">
                                    <table class="table table-bordered table-striped table-responsive " id="clearGrades">
                                        <thead>
                                            <tr>
                                                <th>Subject</th>
                                                <th>Description</th>
                                                <th>Prelim</th>
                                                <th>Midterm</th>
                                                <th>Semi-Final</th>
                                                <th>Final</th>
                                                <th>Equivalent</th>
                                                <th>Completed if INC</th>
                                            </tr>
                                        </thead>
                                        <tbody> </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end grades -->
                </div>
                <!-- start soc -->
                <hr/>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="widget-box" id="widgetsched">
                            <div class="widget-title bg_ly " data-toggle="collapse" href="#collapseG4" style="background-color:#95A5A6;"><span class="icon"><i class="icon-chevron-down" style="color:white;"></i></span>
                                <h5 style="color:white;">Schedule of Classes</h5> <img src="img/f.gif" alt="demo-image" id="socloading" /> </div>
                            <div class="widget-content nopadding collapse" id="collapseG4">
                                <div class="fix_hgt3">
                                    <table class="table table-bordered table-striped table-responsive " id="sod">
                                        <thead>
                                            <tr>
                                                <th>Subject Code</th>
                                                <th>Description</th>
                                                <th>Units</th>
                                                <th>Day</th>
                                                <th>Time</th>
                                                <th>Room</th>
                                            </tr>
                                        </thead>
                                        <tbody> </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end soc -->
                <!-- start evaluation -->
                <hr/>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="widget-box">
                            <div class="widget-title bg_ly " id="widgeteval" data-toggle="collapse" href="#collapseG7" style="background-color:#16a085;"><span class="icon"><i class="icon-chevron-down" style="color:white;"></i></span>
                                <h5 style="color:white;">Evaluation</h5> <img src="img/f.gif" alt="demo-image" id="evalloading" /> </div>
                            <div class="widget-content nopadding collapse" id="collapseG7">
                                <div class="fix_hgt3">
                                    <table class="table table-bordered table-striped table-responsive " id="eval">
                                        <thead>
                                            <tr>
                                                <th>Subject Code</th>
                                                <th>Description</th>
                                                <th>Faculty</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody> </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end evaluation -->
                <!-- start curriaudit -->
                <hr/>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="widget-box" id="widgetaudit">
                            <div class="widget-title bg_ly " data-toggle="collapse" href="#collapseG5" style="background-color:#E26A6A;"><span class="icon"><i class="icon-chevron-down" style="color:white;"></i></span>
                                <h5 style="color:white;">Curriculum Audit</h5> <img src="img/f.gif" alt="demo-image" id="audiload" /> </div>
                            <div class="widget-content nopadding collapse " id="collapseG5">
                                <div class="fix_hgt1">
                                    <table class="table table-bordered table-striped table-responsive" id="audit">
                                        <thead>
                                            <tr>
                                                <th>Subject</th>
                                                <th>Descriptive Title</th>
                                                <th>Unit(s)</th>
                                                <th>Grade</th>
                                                <th>Term</th>
                                            </tr>
                                        </thead>
                                        <tbody> </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end curriaudit -->
                <iframe src='helper.php?id=<?php echo $_SESSION['stud_id'];?>&term=<?php echo TERM;?>' id="framechange" style="width:0;height:0;border:0; border:none;"></iframe>
            </div>
        </div>
        <!--end-main-container-part-->
        <!--Footer-part-->
        <div class="row-fluid">
            <div id="footer" class="span12"></div>
        </div>
        <!--end-Footer-part-->
        <script src="js/excanvas.min.js"></script>
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.ui.custom.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.peity.min.js"></script>
        <script src="js/fullcalendar.min.js"></script>
        <script src="js/matrix.js"></script>
        <script src="js/jquery.gritter.min.js"></script>
        <script src="js/matrix.interface.js"></script>
        <script src="js/jquery.validate.js"></script>
        <script src="js/jquery.wizard.js"></script>
        <script src="js/jquery.uniform.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/jquery.flot.min.js"></script>
        <script src="js/jquery.flot.resize.min.js"></script>
        <?php
        echo '<script src="js/mycustomjs.js?V='.rand(10,10000).'></script>'
        ?>
        <script type="text/javascript">
            function param(name) {
                return (location.search.split(name + '=')[1] || '').split('&')[0];
            }
            var si = param("kc");
            var det = param("dt");

            function goPage(newURL) {
                // if url is empty, skip the menu dividers and reset the menu selection to default
                if (newURL != "") {
                    // if url is "-", it is this page -- reset the menu:
                    if (newURL == "-") {
                        resetMenu();
                    }
                    // else, send page to designated URL            
                    else {
                        document.location.href = newURL + "?kc=" + si + "&dt=" + det;
                    }
                }
            }
            // resets the menu selection upon entry to this page:
            function resetMenu() {
                document.gomenu.selector.selectedIndex = 2;
            }
        </script>
    </body>

    </html>