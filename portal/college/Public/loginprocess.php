<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/student.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/security.php');

if(empty($_SESSION)) // if the session not yet started 
   session_start();

if(isset($_SESSION['idnum']) && isset($_SESSION['pin'])){
    $conn->close();
    header("Location:index.php");
    exit;
}

if(!isset($_POST['submit']) || (empty($_POST['idnum']) || empty($_POST['PIN']))){
    $conn->close();
    header("Location:login.php");
    $_SESSION['invalid_password'] = 'true';
    exit;
}
else{
    
        $idnum = filter_var($_POST['idnum'], FILTER_SANITIZE_STRING);
        $pin = filter_var($_POST['PIN'], FILTER_SANITIZE_STRING);
        $result = check_student_validity($idnum,$pin);
        $conn->close();
    
        if($result)
        {
            $_SESSION['stud_id'] = $idnum;
            $_SESSION['dt'] = date("Y-m-d");
            $ec = my_encrypt($idnum, KEY);
            $date = my_encrypt(date("Y-m-d"), KEY);
            header("Location:index.php?kc={$ec}&dt={$date}");
            exit;
        }
        else{
            $_SESSION['invalid_password'] = 'true';
            header("Location:login.php");
            exit;
        } 
            
}
    

?>