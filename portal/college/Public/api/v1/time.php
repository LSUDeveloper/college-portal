<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/time.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/security.php');




if(isset($_GET['kc'])  && isset($_GET['dt'])){
    $idnum = filter_var($_GET['kc'], FILTER_SANITIZE_STRING);
    $date = filter_var($_GET['dt'], FILTER_SANITIZE_STRING);
    if(date("Y-m-d") === my_decrypt($date,KEY)){
        $key = my_decrypt($idnum, KEY);
        $obj = getTime($key);
        if($obj){
            echo $obj;
        }
        else
            echo "invalid";
    }
    else
    {
        echo "invalid";

    }
}
else
{
    echo "invalid";
}

$conn->close();
exit;
?>