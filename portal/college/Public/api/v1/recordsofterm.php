<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/recordsofterm.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/security.php');




if(isset($_GET['stud_id']) && isset($_GET['dt']))
{
    //ZGRBbTZYM2VtUUdjNGhJb1VZV2xTdz09OjrZ5rC9-8CPqM5Q-snNIZLu
    $idnum = filter_var($_GET['stud_id'], FILTER_SANITIZE_STRING);
    $key = my_decrypt($idnum, KEY);
    $date = filter_var($_GET['dt'], FILTER_SANITIZE_STRING);
    if(date("Y-m-d") === my_decrypt($date,KEY)){

        if(!$key)
            echo "invalid";
        else{
            $terms = check_all_available_terms($key);
            echo $terms->toJSON();
        }
    }
    else
        echo "invalid";
}
else{  
    echo "invalid";
}




$conn->close();   
exit;

?>