<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/curriaudit.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/security.php');


if(isset($_GET['stud_id']) && isset($_GET['dt'])){
    $idnum = filter_var($_GET['stud_id'], FILTER_SANITIZE_STRING);
    $key = my_decrypt($idnum, KEY);  
    $date = filter_var($_GET['dt'], FILTER_SANITIZE_STRING);
    if(date("Y-m-d") === my_decrypt($date,KEY)){
        $newObj = getAudits($key);

        if($newObj)
            echo $newObj->toJSON();
        else
            echo "invalid";

    }
    else
        echo "invalid"; 
}
else
{
    echo "invalid";
}

$conn->close();   
exit;


//$obj = getGrades("11212657","20161");
//echo $obj->toJSON();


?>