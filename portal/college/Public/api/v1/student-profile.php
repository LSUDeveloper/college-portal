<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/student.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/security.php');



//echo "<br/>".base64_url_encode(openssl_random_pseudo_bytes(32))."<br/>";

if(isset($_GET['kc'])  && isset($_GET['dt'])){
    $idnum = filter_var($_GET['kc'], FILTER_SANITIZE_STRING);
    $date = filter_var($_GET['dt'], FILTER_SANITIZE_STRING);
    if(date("Y-m-d") === my_decrypt($date,KEY)){
        $key = my_decrypt($idnum, KEY);
        $obj = getInfo($key);
        if($obj){
            echo $obj->toJSON();
        }
        else
            echo "invalid";
    }
    else
    {
        echo "invalid";

    }
}
else
{
    echo "invalid";
}

//echo "<br/>".hash_hmac('ripemd160', 'The quick brown fox jumped over the lazy dog.', 'qwdqwdqwdwq');

$conn->close();
exit;
?>