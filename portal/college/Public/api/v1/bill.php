<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/bill.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/security.php');


if(isset($_GET['period']) && isset($_GET['stud_id']) && isset($_GET['dt'])){
    $idnum = filter_var($_GET['stud_id'], FILTER_SANITIZE_STRING);
    $period = filter_var($_GET['period'], FILTER_SANITIZE_STRING);
    $date = filter_var($_GET['dt'], FILTER_SANITIZE_STRING);
    if(date("Y-m-d") === my_decrypt($date,KEY)){
        $key = my_decrypt($idnum, KEY);
        if($period != 0){  
            $newObj = getBill($key,$period);
            if($newObj)
                echo $newObj->toJSON();
            else
                echo "invalid";
        } 
        else
            echo "invalid";
    }
    else{
        echo "invalid";
    }
}
else{
    echo "invalid";
}


$conn->close();   
exit;
?>