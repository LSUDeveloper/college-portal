<?php
session_start();
unset($_SESSION["stud_id"]);
session_unset();
session_destroy();

header("Location: login.php");
exit;
?>