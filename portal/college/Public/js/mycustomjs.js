    function param(name) {
        return (location.search.split(name + '=')[1] || '').split('&')[0];
    }

function objectValues(obj) {
    var res = [];
    for (var i in obj) {
        if (obj.hasOwnProperty(i)){
            res.push(obj[i]);
        }
    }
    return res;
}
    var si = param("kc");
    var det = param("dt");
    var studnum = "";
    var ctterm = 0;
    var hashed = 0;
    $.ajax({
        url: "/portal/college/Public/api/v1/student-profile.php",
        type: "GET",
        data: {
            kc: si,
            dt: det
        },
        cache: false,
        success: function (data) {
            if (data != "invalid") {
                var pt = $("#profiletbl tbody");
                var decoded = JSON.parse(data);
                var img = "data:image/png;base64," + decoded.img;
                $("#profilepic").attr("src", img);
                pt.empty();
                hashed = decoded.hashed;
                ctterm = decoded.term;
                var idnumber = "<tr><td><b>ID #</b></td></tr>";
                idnumber += "<tr><td>" + decoded.idnum + "</td></tr>";
                studnum = decoded.idnum;
                pt.append(idnumber);
                //------ADDING NOTIFICATION
               // var sdasd = $("#widget-notif");
               // var qwdqwdwq = '<div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading">Online Pre-registration for old students only</h4> Online Pre-registration for old students only is now available Please <a href="http://local.lsu.edu.ph/enroll/register2.php?id='+decoded.idnum+'"><u> CLICK THIS LINK</u> </a> </div>'
               // sdasd.append(qwdqwdwq);
                var name = "<tr><td><b>NAME</b></td></tr>";
                name += "<tr><td>" + decoded.name + "</td></tr>";
                pt.append(name);
                var course = "<tr><td><b>COURSE</b></td></tr>";
                course += "<tr><td>" + decoded.course + "</td></tr>";
                pt.append(course);
                var dob = "<tr><td><b>Date of Birth</b></td></tr>";
                dob += "<tr><td>" + decoded.dob + "</td></tr>";
                pt.append(dob);
                var gender = "<tr><td><b>Gender</b></td></tr>";
                gender += "<tr><td>" + decoded.gender + "</td></tr>";
                pt.append(gender);
                $("#profname").text(decoded.name);
                var card = $("#card td[class='taskStatus'] span");
                if (decoded.card == 1) {
                    card.attr("class", "done");
                    card.html("<i class='icon-ok' style='font-size:25px;'></i>");
                }
                var hondismissal = $("#hondismissal td[class='taskStatus'] span");
                if (decoded.hondismissal == 1) {
                    hondismissal.attr("class", "done");
                    hondismissal.html("<i class='icon-ok' style='font-size:25px;'></i>");
                }
                var goodmoral = $("#goodmoral td[class='taskStatus'] span");
                if (decoded.goodmoral == 1) {
                    goodmoral.attr("class", "done");
                    goodmoral.html("<i class='icon-ok' style='font-size:25px;'></i>");
                }
                var nso = $("#nso td[class='taskStatus'] span");
                if (decoded.nso == 1) {
                    nso.attr("class", "done");
                    nso.html("<i class='icon-ok' style='font-size:25px;'></i>");
                }
                var form137 = $("#form137 td[class='taskStatus'] span");
                if (decoded.f137a == 1) {
                    form137.attr("class", "done");
                    form137.html("<i class='icon-ok' style='font-size:25px;'></i>");
                }
                var tor = $("#tor td[class='taskStatus'] span");
                if (decoded.tor == 1) {
                    tor.attr("class", "done");
                    tor.html("<i class='icon-ok' style='font-size:25px;'></i>");
                }
                // $(".mainpart").fadeIn(5000);
            }
        },
        complete: function () {
            //$('#check').text("qweqwe");
        }
    });
    $(document).ready(function () {
        function numberWithCommas(n) {
            var parts = n.toString().split(".");
            return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
        }
        var socloading = $("#socloading");
        var audiload = $("#audiload");
        var soaloading = $("#soaloading");
        var clearanceloading = $("#clearanceloading");
        var evalloading = $("#evalloading");
        var clearancegrading = $("#clearancegrading");
        var billloading = $("#billloading");

        evalloading.hide();
        socloading.hide();
        audiload.hide();
        soaloading.hide();
        clearanceloading.hide();
        clearancegrading.hide();
        billloading.hide();
        var checkerforTermWidget = 0;
        var checkerforTermAudit = 0;
        var checkerforSched = 0;
        var checkerforEval = 0;
        var qwf = 0;
        var myEventForBill = function (e) {
            var billsce = $("input[name='quarter']:checked");
            var selected = billsce.val();

            billloading.show();
            $.ajax({
                url: "/portal/college/Public/api/v1/bill.php",
                type: "GET",
                data: {
                    stud_id: si,
                    period: selected,
                    dt: det
                },
                cache: false,
                success: function (data) {
                    var tb = $("#tblbill");
                    var c1 = $(tb).children("tbody").eq(0);
                    var tds = $(c1).children("tr");

                    if (data != "invalid") {

                        var ic = 1;
                        var decoded = JSON.parse(data);
                        var test = objectValues(decoded);
                        $(tds).each(function () {
                            $(this).children("td").eq(1).text(numberWithCommas(parseInt(test[ic++]).toFixed(2)));
                            if (ic == test.length)
                                $(this).children("td").eq(0).html("<b>" + test[0] + " Amount Due</b>");

                        });

                    } else {
                        $(tds).each(function () {
                            $(this).children("td").eq(1).text("0");
                        });
                        alert("The bill for this current quarter is still not available!");
                    }
                },
                complete: function () {
                    //$('#check').text("qweqwe");
                    billloading.hide();
                }
            });
        }
        var myEventForTerm = function (e) {
            if (checkerforTermWidget == 0) {
                checkerforTermWidget++;
                var widget = $(this);
                var select = $(this).find("select[name='Term']").eq(0);
                $.ajax({
                    url: "/portal/college/Public/api/v1/recordsofterm.php",
                    type: "GET",
                    data: {
                        stud_id: si,
                        dt: det
                    },
                    cache: false,
                    success: function (data) {
                        if (data != "invalid") {
                            var decoded = JSON.parse(data);
                            $.each(decoded.Terms, function (key, value) {
                                var semester = parseInt(value.charAt(4));
                                switch (semester) {
                                    case 1:
                                        semester = "1st Semester ";
                                        break;
                                    case 2:
                                        semester = "2nd Semester ";
                                        break;
                                    case 0:
                                        semester = "Summer ";
                                        break;
                                    default:
                                        break;
                                }
                                var year = parseInt(value.substring(0, 4));
                                var mutatedValue = semester + year + "-" + (year + 1);
                                select.append($('<option/>', {
                                    value: value
                                }).text(mutatedValue));
                            });
                        }
                    },
                    complete: function () {
                        //$('#check').text("qweqwe");
                    }
                });
            }
        }
        var myEventforGrades = function (e) {
            var selectedterms = $('select[name="Term"]');
            var selected = selectedterms.find('option:selected').attr("value");

            var table = $('#clearGrades tbody');
            clearancegrading.show();
            //alert(selected);
            if (selected != 0) {
                $.ajax({
                    url: "/portal/college/Public/api/v1/grades.php",
                    type: "GET",
                    data: {
                        stud_id: si,
                        term: selected,
                        dt: det
                    },
                    cache: false,
                    success: function (data) {
                        if (data != "invalid") {
                            var decoded = JSON.parse(data);
                            var test = objectValues(decoded);
                            table.empty();
                            $.each(test, function (key, value) {
                                var index = objectValues(value);
                                var row = "<tr>";
                                $.each(index, function (key, value) {
                                    var truevalue = value != null ? value : '';
                                    row += "<td style='text-align:center;'>" + truevalue + "</td>";
                                });
                                row += "</tr>";
                                table.append(row);
                            });
                        }
                    },
                    complete: function () {
                        clearancegrading.hide();
                        //$('#check').text("qweqwe");
                    }
                });
            }
        }
        var myEventforSched = function (e) {
            if (checkerforSched == 0) {
                checkerforSched++;
                var table = $('#sod tbody');
                socloading.show();
                $.ajax({
                    url: "/portal/college/Public/api/v1/schedule.php",
                    type: "GET",
                    data: {
                        stud_id: si,
                        dt: det
                    },
                    cache: false,
                    success: function (data) {
                        socloading.hide();
                        if (data != "invalid") {
                            var decoded = JSON.parse(data);
                            var test = objectValues(decoded);
                            table.empty();
                            $.each(test, function (key, value) {
                                var index = objectValues(value);
                                var row = "<tr>";
                                $.each(index, function (key, value) {
                                    var truevalue = value != null ? value : '';
                                    row += "<td style='text-align:center;'>" + truevalue + "</td>";
                                });
                                row += "</tr>";
                                table.append(row);
                            });
                        }
                    },
                    complete: function () {
                        //$('#check').text("qweqwe");
                    }
                });
            }
        }
        var myEventForEvaluation = function (e) {
            //alert("data");
            if (checkerforEval == 0) {
                checkerforEval++;
                var table = $('#eval tbody');
                evalloading.show();
                $.ajax({
                    url: "/portal/college/Public/api/v1/evaluation.php",
                    type: "GET",
                    data: {
                        stud_id: si,
                        dt: det
                    },
                    cache: false,
                    success: function (data) {
                        evalloading.hide();
                        if (data != "invalid") {
                            var decoded = JSON.parse(data);
                            var test = objectValues(decoded);
                            table.empty();
                            $.each(test, function (key, value) {
                                var index = objectValues(value);
                                var row = "<tr>";
                                var scode = value.Subject;
                                var fac = value.Facid;
                                //alert(scode + " "  + fac);
                                $.each(index, function (key, value) {
                                    var truevalue = value != null ? value : '';
                                    if (key < 3) {
                                        row += "<td style='text-align:center;'>" + truevalue + "</td>";
                                    } else {
                                        if (key == 4) {
                                            if (value == "false") {
                                                var link = "term=" + ctterm + "&id=" + studnum + "&code=" + scode + "&facid=" + fac + "&s=" + hashed;
                                                row += "<td style='text-align:center;'> <a href=\"http://203.177.51.125:88/facultyevaluation/SEOCF/index.php?" + link + "\" class=\"btn btn-info\" role=\"button\">Evaluate</a> </td> ";
                                            } else {
                                                row += "<td style='text-align:center;'><button class='btn btn-danger' >Evaluated</button</td>";
                                            }
                                        }

                                    }
                                });
                                row += "</tr>";
                                table.append(row);
                            });
                        }
                    },
                    complete: function () {
                        //$('#check').text("qweqwe");
                    }
                });
            }
        }
        var myEventforAudit = function (e) {
            if (checkerforTermAudit == 0) {
                checkerforTermAudit++;
                audiload.show();
                var table = $('#audit tbody');
                $.ajax({
                    url: "/portal/college/Public/api/v1/curriaudit.php",
                    type: "GET",
                    data: {
                        stud_id: si,
                        dt: det
                    },
                    cache: false,
                    success: function (data) {
                        audiload.hide();
                        //alert(data);
                        if (data != "invalid") {
                            var decoded = JSON.parse(data);
                            table.empty();
                            $.each(decoded, function (key, value) {
                                var year = "",
                                    semester = "";
                                var arr = key.split("-");
                                //alert(arr[0] + " " + arr[1]);
                                switch (arr[0]) {
                                    case "I":
                                        year = "First";
                                        break;
                                    case "II":
                                        year = "Second";
                                        break;
                                    case "III":
                                        year = "Third";
                                        break;
                                    case "IV":
                                        year = "Fourth";
                                        break;
                                    default:
                                        year = "Fifth";
                                        break;
                                }
                                year += " Year";
                                switch (arr[1]) {
                                    case "1":
                                        semester = "1st Semester";
                                        break;
                                    default:
                                        semester = "2nd Semester";
                                        break;
                                }
                                semester = year + " - " + semester;
                                var header = '<tr><th colspan="5">' + semester + '</th></tr>'
                                table.append(header);
                                var index = objectValues(value);
                                $.each(index, function (key, value) {
                                    var fullrow = "<tr>";
                                    var index2 = objectValues(value);
                                    $.each(index2, function (key, value) {
                                        fullrow += "<td>" + value + "</td>";
                                    });
                                    fullrow += "</tr>";
                                    table.append(fullrow);
                                });
                            });
                        }
                    },
                    complete: function () {
                        //$('#check').text("qweqwe");
                    }
                });
            }
        }
        var checkforSoa = 0;
        var MyEventForSoa = function (e) {
            if (checkforSoa == 0) {
                soaloading.show();
                checkforSoa++;
                var table = $('#tblsoa tbody');
                $.ajax({
                    url: "/portal/college/Public/api/v1/soa.php",
                    type: "GET",
                    data: {
                        kc: si,
                        dt: det
                    },
                    cache: false,
                    success: function (data) {
                        soaloading.hide();
                        if (data != "invalid") {
                            var decoded = JSON.parse(data);
                            $("#pa").text(numberWithCommas(decoded.account[0].toFixed(2)));
                            $("#tf").text(numberWithCommas(decoded.account[1].toFixed(2)));
                            $("#lf").text(numberWithCommas(decoded.account[2].toFixed(2)));
                            $.each(decoded.subjects, function (key, value) {
                                var a = "<tr><td style='text-align:center;'>";
                                a += key + "</td><td style='text-align:right;'>";
                                a += numberWithCommas(value[0].toFixed(2));
                                a += "</td></tr>";
                                $("#tfs").after(a);
                                if (value[1] != 0) {
                                    a = "<tr><td style='text-align:center;'>";
                                    a += key + "</td><td style='text-align:right;'>";
                                    a += numberWithCommas(value[1].toFixed(2));
                                    a += "</td></tr>";
                                    $("#lfs").after(a);
                                }
                            });
                        }
                        $("#sf").text(numberWithCommas(decoded.account[3].toFixed(2)));
                        $("#of").text(numberWithCommas(decoded.account[4].toFixed(2)));
                        $("#dis").text(numberWithCommas(decoded.account[5].toFixed(2)));
                        var discount = 0;
                        $.each(decoded.discount, function (key, value) {
                            var a = "<tr><td style='text-align:center;'>";
                            a += key + "</td><td style='text-align:right;'>";
                            a += numberWithCommas(value.toFixed(2));
                            discount += value;
                            a += "</td></tr>";
                            $("#diss").after(a);
                        });
                        $.each(decoded.adss, function (key, value) {
                            var a = "<tr><td style='text-align:center;'>";
                            a += key + "</td><td style='text-align:right;'>";
                            a += numberWithCommas(value.toFixed(2));
                            discount += value;
                            a += "</td></tr>";
                            $("#ofss").after(a);
                        });
                        var payments = 0;
                        $.each(decoded.payments, function (key, value) {
                            var a = "<tr><td style='text-align:center;'>";
                            a += value[0] + "</td><td style='text-align:right;'>";
                            a += numberWithCommas(value[1].toFixed(2));
                            payments += value[1];
                            a += "</td></tr>";
                            $("#pays").after(a);
                        });
                        $("#pay").text(numberWithCommas(payments.toFixed(2)));
                        $("#bal").text(numberWithCommas((decoded.account[6] - (payments + discount)).toFixed(2)));
                    },
                    complete: function () {
                        //$('#check').text("qweqwe");
                    }
                });
            }
        }
        var eventForClearance = function (e) {
            $.ajax({
                url: "/portal/college/Public/api/v1/clearance.php",
                type: "GET",
                data: {
                    kc: si,
                    dt: det
                },
                cache: false,
                success: function (data) {
                    if (data != "invalid") {
                        var tbl = $("#tblclearance tbody");
                        var decoded = JSON.parse(data);
                        var term = decoded.Term;
                        var period = decoded.Period;
                        var semester;
                        switch (period) {
                            case 0:
                                period = " Prelim";
                                break;
                            case 1:
                                period = " Midterm";
                                break;
                            case 2:
                                period = " Semi-Final";
                                break;
                            default:
                                period = " Final";
                                break;
                        }
                        switch (term.charAt(4)) {
                            case "1":
                                semester = "1st Semester ";
                                break;
                            default:
                                semester = "2nd Semester ";
                                break;
                        }
                        var year = parseInt(term.substring(0, 4));
                        var complete = semester + "of " + year + "-" + (year + 1) + period;
                        $("#clearancesemester").text(complete);
                        tbl.empty();
                        if (decoded.clearance) {
                            $.each(decoded.clearance, function (key, value) {
                                var check = "";
                                if (value.status === 1) check = "<span class='pending'><i class='icon-remove-sign' style='font-size:45px;'></i></span>";
                                else check = "<span class='done'><i class='icon-ok' style='font-size:45px;'></i></span>";
                                var com = "<tr><td>" + value.office + "</td><td>" + value.remarks + "</td><td class='taskStatus'>" + check + "</td></tr>";
                                tbl.append(com);
                            });
                        }
                    }
                },
                complete: function () {
                    //$('#check').text("qweqwe");
                }
            });
        }
        String.prototype.toHHMMSS = function () {
                var sec_num = parseInt(this, 10); // don't forget the second param
                var hours = Math.floor(sec_num / 3600);
                var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
                var seconds = sec_num - (hours * 3600) - (minutes * 60);
                if (hours < 10) {
                    hours = "0" + hours;
                }
                if (minutes < 10) {
                    minutes = "0" + minutes;
                }
                if (seconds < 10) {
                    seconds = "0" + seconds;
                }
                return hours + ':' + minutes;
            }
            //var notif = function (e) {
        $.ajax({
            url: "/portal/college/Public/api/v1/time.php",
            type: "GET",
            data: {
                kc: si,
                dt: det
            },
            cache: false,
            success: function (data) {
                if (data === "invalid") {
                    $("#lan").text("00:00 hr(s)");
                    $("#wlan").text("00:00 hr(s)");
                } else {
                    var lan = (parseInt(data)) + "";
                    var wlan = (parseInt(data) * 2) + "";
                    $("#lan").text(lan.toHHMMSS() + " hr(s)");
                    $("#wlan").text(wlan.toHHMMSS() + " hr(s)");
                }
            },
            complete: function () {
                //$('#check').text("qweqwe");
            }
        });
        /*$.ajax({
            url: "/portal/college/Public/api/v1/notifications/notification.php",
            type: "GET",
            cache: false,
            success: function (data) {
                var decoded = JSON.parse(data);
                $.each(decoded, function (key, value) {
                    var com = '<div class="alert alert-success alert-block"><h4 class="alert-heading">' + value.title + '</h4>' + value.message + "</div>";
                    $("#widget-notif").append(com);
                });
            },
            complete: function () {
                //$('#check').text("qweqwe");
            }
        });*/
        //}
        $("#widgetterm").on("click", myEventForTerm);
        $("#widgetsoa").on("click", MyEventForSoa);
        $("#widgeteval").on("click", myEventForEvaluation);
        $("#widgetsched").on("click", myEventforSched);
        $("#widgetaudit").on("click", myEventforAudit);
        $("#btnBill").on("click", myEventForBill);
        $('#btnGrades').on("click", myEventforGrades);
        $("#widgetclearance").on("click", eventForClearance);
        /* $("button").click( function (){
             alert("Hello");
         });*/
        // $("#tblbill").toggle(myEvent);
    });