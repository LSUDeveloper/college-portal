<?php


if(!isset($_SESSION))
    session_start();

if(!isset($_SESSION["stud_id"]) || !isset($_GET["kc"]) || !isset($_GET["dt"]))
{
    header("Location:login.php");
    exit;
}

if(!(date("Y-m-d") === $_SESSION['dt'])){
    header("Location:login.php");
    exit;
}


?>
<!DOCTYPE html>
    <html lang="en">

    <head>
        <title>College Portal - LSUOz</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="shortcut icon" type="image/x-icon" href="img/icon.png" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="css/uniform.css" />
        <link rel="stylesheet" href="css/select2.css" />
        <link rel="stylesheet" href="css/matrix-style.css" />
        <link rel="stylesheet" href="css/matrix-media.css" />
        <link rel="stylesheet" href="css/custom.css" />
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/jquery.gritter.css" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'> </head>

    <body>
        <!--Header-part-->
        <div id="header">
            <h1><a href="dashboard.html">Matrix Admin</a></h1> </div>
        <!--close-Header-part-->
        <!--top-Header-menu-->
        <div id="user-nav" class="navbar navbar-inverse">
            <ul class="nav">
                <li class=""><img class="test" src="img/alternative.jpg"  id="profilepic" /><span id="profname" class="text"></span></li>
                <li class=""><a title="" href="logout.php"><i class="icon icon-share-alt"></i><span class="text"> Logout</span></a></li>
            </ul>
        </div>
        <!--close-top-Header-menu-->
        <!--start-top-serch-->
        <!--close-top-serch-->
        <!--sidebar-menu-->
        <div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
            <ul>
                <li><a href="javascript:goPage('index.php')"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
                <li class="active"> <a href="#"><i class="icon icon-signal"></i> <span>Forms</span></a> </li>
            </ul>
        </div>
        <!--sidebar-menu-->
        <!--main-container-part-->
        <div id="content">
            <!--breadcrumbs-->
            <div id="content-header">
                <div id="breadcrumb"><a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Forms</a></div>
                <h1>Forms</h1> </div>
            <!--End-breadcrumbs-->
            <!--Chart-box-->
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="widget-box">
                            <div class="widget-title bg_ly " data-toggle="collapse" href="#collapse3"><span class="icon"><i class="icon-chevron-down"></i></span>
                                <h5>Application for Graduation</h5> </div>
                            <div class="widget-content nopadding collapse" id="collapse3">
                                <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSc08_ojtNU8pajFBwv7gOzGEjN5Dana2mvilAuLDC_r88ZaGw/viewform" width="100%" height="500px"> </iframe>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="widget-box">
                                <div class="widget-title bg_ly " data-toggle="collapse" href="#collapse1"><span class="icon"><i class="icon-chevron-down"></i></span>
                                    <h5>University-Tshirt</h5> </div>
                                <div class="widget-content nopadding collapse" id="collapse1">
                                    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScJeKD7eObc-fzKZzcap3clyxHqVp5mjTUX4372TExjyWwHew/viewform?embedded=true" width="100%" height="500px"> </iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <!--end-main-container-part-->
            <!--Footer-part-->
            <div class="row-fluid">
                <div id="footer" class="span12"></div>
            </div>
            <!--end-Footer-part-->
            <script src="js/excanvas.min.js"></script>
            <script src="js/jquery.min.js"></script>
            <script src="js/jquery.ui.custom.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/jquery.peity.min.js"></script>
            <script src="js/fullcalendar.min.js"></script>
            <script src="js/matrix.js"></script>
            <script src="js/jquery.gritter.min.js"></script>
            <script src="js/matrix.interface.js"></script>
            <script src="js/jquery.validate.js"></script>
            <script src="js/matrix.form_validation.js"></script>
            <script src="js/jquery.wizard.js"></script>
            <script src="js/jquery.uniform.js"></script>
            <script src="js/select2.min.js"></script>
            <script src="js/jquery.dataTables.min.js"></script>
            <script src="js/matrix.tables.js"></script>
            <script src="js/jquery.flot.min.js"></script>
            <script src="js/jquery.flot.resize.min.js"></script>
            <script type="text/javascript">
                function param(name) {
                    return (location.search.split(name + '=')[1] || '').split('&')[0];
                }
                var si = param("kc");
                var det = param("dt");

                function goPage(newURL) {
                    // if url is empty, skip the menu dividers and reset the menu selection to default
                    if (newURL != "") {
                        // if url is "-", it is this page -- reset the menu:
                        if (newURL == "-") {
                            resetMenu();
                        }
                        // else, send page to designated URL            
                        else {
                            document.location.href = newURL + "?kc=" + si + "&dt=" + det;
                        }
                    }
                }
                // resets the menu selection upon entry to this page:
                function resetMenu() {
                    document.gomenu.selector.selectedIndex = 2;
                }
            </script>
            <script>
                $(document).ready(function () {

                    $.ajax({
                        url: "/portal/college/Public/api/v1/student-profile.php"
                        , type: "GET"
                        , data: {
                            kc: si
                            , dt: det
                        }
                        , cache: false
                        , success: function (data) {
                            if (data != "invalid") {
                                var decoded = JSON.parse(data);
                                var img = "data:image/png;base64," + decoded.img;
                                $("#profilepic").attr("src", img);
                                $("#profname").text(decoded.name);
                                // $(".mainpart").fadeIn(5000);
                            }
                        }
                        , complete: function () {
                            //$('#check').text("qweqwe");
                        }
                    });
                });
            </script>
    </body>

    </html>