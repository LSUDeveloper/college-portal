<?php


require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/config/db.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/config/db2.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/classes/clearance.php');

$query = "SELECT * FROM tbltermstatus ORDER BY fldterm DESC LIMIT 1;";
$results = $conn->query($query);

if($results->num_rows){
    while($r = $results->fetch_assoc()){
        define("TERM",$r['fldcurrentterm']);   
        define("PERIOD","3");
    }
}


function getClearance($idnumber,$term = TERM,$period = PERIOD){
    global $conn2;
    $query = "Select A.fldoffice,A.fldremarks,A.fldstatus from tblclearance as A where A.fldidnumber = ? and A.fldterm = ? and A.fldperiod = ?";

    if($stmt = $conn2->prepare($query)){
        $stmt->bind_param("sss",$idnumber,$term,$period);
        $stmt->execute();
        $stmt->bind_result($a,$b,$c);
        $obj = new clearance($term,$period);
        while($stmt->fetch()){
            $obj->add_clearance(utf8_encode($a),utf8_encode($b),$c);
        }
        return $obj;
    } 

    return false;
}






?>