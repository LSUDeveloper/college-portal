<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/globalfunctions.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/classes/schedule.php');




function getSched($stud_id,$term = TERM){
    global $conn;
$query = "SELECT 
    A.fldsubject,
    B.flddescription,
    B.fldassesmentunit,
    A.fldday1,
    A.fldday2,
    A.fldtime1,
    A.fldtime2,
    A.fldroom1,
    A.fldroom2
FROM
    (SELECT 
        B.fldsubject,
            B.fldday1,
            B.fldday2,
            CONCAT(B.fldstarttime1, ' - ', B.fldendtime1) AS fldtime1,
            CONCAT(B.fldstarttime2, ' - ', B.fldendtime2) AS fldtime2,
            B.fldroom1,
            B.fldroom2
    FROM
        (SELECT 
        A.fldsubjectcode
    FROM
        tblsubjectstaken AS A
    WHERE
        A.fldidnumber = ?
            AND A.fldterm = ?) AS A
    INNER JOIN tblsubjectoffered AS B ON A.fldsubjectcode = B.fldsubjectcode
    WHERE
        B.fldterm = ?) AS A
        INNER JOIN
    tblsubjects AS B ON A.fldsubject = B.fldsubject;";
    
if($stmt = $conn->prepare($query)){
        $stmt->bind_param("sss", $stud_id,$term,$term);
        $stmt->execute();
        $stmt->bind_result($a,$b,$c,$d1,$d2,$t1,$t2,$r1,$r2);
        $wat = new SOD();
        while($stmt->fetch()){
            $wat->add_class(utf8_encode($a),utf8_encode($b),$c,$d1,$d2,$t1,$t2,$r1,$r2);
        }        
        return $wat;       
    }

}

?>