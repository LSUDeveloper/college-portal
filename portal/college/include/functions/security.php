<?php

define("KEY", "ezH_lP2RHWxcpKRVNqeafD78CTDwasB3Szd1nTWuGv4~");

function my_encrypt($data, $key) {
    
    $encryption_key = base64_url_decode($key);
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
   // echo "<br/> -> ".$encrypted."<br/>".$iv."<br/>";
    return base64_url_encode($encrypted . '::' . $iv);
}
 
function my_decrypt($data, $key) {
    $encryption_key = base64_url_decode($key);
    list($encrypted_data, $iv) = array_pad(explode('::', base64_url_decode($data), 2), -2, null);
    $ic = @openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    return $ic;

}


function base64_url_encode($input) {
 return strtr(base64_encode($input), '+/=', '-_~');
}

function base64_url_decode($input) {
 return base64_decode(strtr($input, '-_~', '+/='));
}

?>