<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/globalfunctions.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/classes/curriaudit.php');


//11212657

function getAudits($stud_id){
    global $conn;
    $query = "SELECT 
    A.fldsubject,
    A.flddescription,
    A.fldterm,
    A.fldassesmentunit,
    CASE
        WHEN B.fldgrade IS NULL THEN ''
        ELSE B.fldgrade
    END AS fldgrade,
    (CASE
        WHEN
            B.fldterm IS NOT NULL
        THEN
            CONCAT((CASE
                        WHEN SUBSTRING(B.fldterm, 5, 1) = 1 THEN '1st Sem'
                        WHEN SUBSTRING(B.fldterm, 5, 1) = 2 THEN '2nd Sem'
                        ELSE 'Summer'
                    END),
                    ' ',
                    SUBSTRING(B.fldterm, 1, 4))
        ELSE ''
    END) AS fldterm
FROM
    (SELECT 
        A.fldsubject,
            A.fldterm,
            B.flddescription,
            B.fldassesmentunit
    FROM
        (SELECT 
        B.fldsubject, B.fldterm
    FROM
        tblstudent AS A
    INNER JOIN tblprospectus AS B ON A.fldprospectuscode = B.fldprospectuscode
    WHERE
        A.fldidnumber = ?) AS A
    INNER JOIN tblsubjects AS B ON A.fldsubject = B.fldsubject) AS A
        LEFT JOIN
    (SELECT 
        B.fldsubject, B.fldsubjectcode, A.fldgrade, A.fldterm
    FROM
        tblsubjectstaken AS A
    INNER JOIN tblsubjectoffered AS B ON A.fldsubjectcode = B.fldsubjectcode
        AND A.fldterm = B.fldterm
    WHERE
        A.fldidnumber = ?) AS B ON A.fldsubject = B.fldsubject
ORDER BY A.fldterm , A.flddescription;";

    if($stmt = $conn->prepare($query)){
        $stmt->bind_param("ss", $stud_id,$stud_id);
        $stmt->execute();
        $stmt->bind_result($a,$b,$c,$d,$e,$f);
        $wat = new curriaudit();
        while($stmt->fetch()){
            $wat->add_subject($a,utf8_encode($b),$c,$d,$e,$f);
        }        
        return $wat;       
    }

}

?>