<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/config/db.php');

$query = "SELECT * FROM tbltermstatus ORDER BY fldterm DESC LIMIT 1;";
$results = $conn->query($query);

if($results->num_rows){
    while($r = $results->fetch_assoc()){
        define("TERM",$r['fldcurrentterm']);     
    }
}


function check_student_validity($id_num, $pin, $term = TERM){
    global $conn;
    $query = "SELECT 
    A.fldidnumber, A.fldpin
FROM
    tblstudent AS A
        INNER JOIN
    tblcourses AS B ON TRIM(A.fldcourse) = TRIM(B.fldcourse)
WHERE
    (B.flddepartmentcode = 'CAS'
        OR B.flddepartmentcode = 'CBE'
        OR B.flddepartmentcode = 'CCS'
        OR B.flddepartmentcode = 'CED'
        OR B.flddepartmentcode = 'COA'
        OR B.flddepartmentcode = 'COE'
        OR B.flddepartmentcode = 'CON'
        OR B.flddepartmentcode = 'CAS'
        OR B.flddepartmentcode = 'SHM'
        OR B.flddepartmentcode = 'GRAD')
        AND A.fldidnumber = ?
        AND A.fldpin = ?
LIMIT 1;";
    
    if($stmt = $conn->prepare($query)){
        $stmt->bind_param("ss",$id_num, $pin);
        $stmt->execute();
        //$stmt->bind_result($idnum);

        while($stmt->fetch()){
            $stmt->free_result();
            return true;
        }
        return false;
    }
}





?>