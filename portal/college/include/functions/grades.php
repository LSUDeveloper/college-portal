<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/globalfunctions.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/classes/grades.php');




function getGrades($stud_id,$term = TERM){
    global $conn;
$query = "SELECT 
    A.fldsubject,
    B.flddescription,
    A.fldprelimg,
    A.fldmidtermg,
    A.fldsemifinal,
    A.fldfinalg,
    A.fldgrade,
    A.fldcompleteg
FROM
    (SELECT 
        B.fldsubject,
            A.fldprelimg,
            A.fldmidtermg,
            A.fldsemifinal,
            A.fldfinalg,
            A.fldgrade,
            A.fldcompleteg
    FROM
        (SELECT 
        A.fldsubjectcode,
            A.fldprelimg,
            A.fldmidtermg,
            A.fldsemifinal,
            A.fldfinalg,
            A.fldgrade,
            A.fldcompleteg
    FROM
        tblsubjectstaken AS A
    WHERE
        A.fldidnumber = ?
            AND A.fldterm = ?) AS A
    INNER JOIN tblsubjectoffered AS B ON A.fldsubjectcode = B.fldsubjectcode
    WHERE
        B.fldterm = ?) AS A
        INNER JOIN
    tblsubjects AS B ON A.fldsubject = B.fldsubject;";
    
if($stmt = $conn->prepare($query)){
        $stmt->bind_param("sss", $stud_id,$term,$term);
        $stmt->execute();
        $stmt->bind_result($a,$b,$c,$d,$e,$f,$g,$h);
        $wat = new Grade();
        while($stmt->fetch()){
            $wat->add_grades(utf8_encode($a),utf8_encode($b),$c,$d,$e,$f,$g,$h);
        }        
        return $wat;       
    }

}






?>