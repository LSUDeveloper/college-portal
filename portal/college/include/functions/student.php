<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/globalfunctions.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/classes/student.php');



function getInfo($key ,$term = TERM){
    global $conn;
    $query = "Select fldidnumber,Concat(fldlname,', ', fldfname) ,fldcourse,fldpic1,flddob,fldgender,fldcard,fldhondismissal,fldgoodmoral,fldnso,fldf137a,fldtor FROM tblstudent where fldidnumber = ? limit 1;";
    if($stmt = $conn->prepare($query)){
        $stmt->bind_param("s", $key);
        $stmt->execute();
        $stmt->bind_result($idnum,$lname,$course,$pic1,$dob,$gender,$card,$hondis,$goodmoral,$nso,$f137a,$tor);
        
        while($stmt->fetch()){
             return new Student($term,$idnum,utf8_encode($lname),utf8_encode($course),$pic1,$dob,$gender,$card,$hondis,$goodmoral,$nso,$f137a,$tor);
        }        
        return false;       
    }

}



?>