<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/globalfunctions.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/classes/recordsofterm.php');

function check_all_available_terms($stud_id,$term = TERM){
global $conn;
$query = "SELECT 
    A.fldterm
FROM
    tblstudentenrolled AS A
WHERE
    A.fldidnumber = ?
    Order By A.fldterm DESC;
";
    
 if($stmt = $conn->prepare($query)){
        $stmt->bind_param("s", $stud_id);
        $stmt->execute();
        $stmt->bind_result($id);
        $a = new rot();
        while($stmt->fetch()){
             $a->add($id);
        }        
        return $a;   
    }
}

?>