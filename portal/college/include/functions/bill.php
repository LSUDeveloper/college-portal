<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/globalfunctions.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/classes/bill.php');


function getBill($stud_id,$period,$term = TERM){
    global $conn;
    $query = "Select A.fldprevaccount, A.fldtuitionfee, A.fldlaboratory, A.fldschoolfees, A.fldotherfees, A.fldadditionalfee, A.fldonetimefee, A.fldpayments, A.flddiscount, A.fldamountdue FROM tblbill as A inner join tblstudentenrolled as B on A.fldidnumber = B.fldidnumber where A.fldidnumber = ? and B.fldidnumber = ? and A.fldperiod = ? and A.fldterm = ? and B.fldterm = ? limit 1;";
    if($stmt = $conn->prepare($query)){
        $stmt->bind_param("sssss", $stud_id,$stud_id,$period,$term,$term);
        $stmt->execute();
        $stmt->bind_result($a,$b,$c,$d,$e,$f,$g,$h,$j,$i);
        
        while($stmt->fetch()){
             return new Bill($period,$a,$b,$c,$d,$e,$f,$g,$h,$j,$i);
        }        
        return false;       
    }

}

?>