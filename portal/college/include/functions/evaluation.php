<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/globalfunctions.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/classes/evaluation.php');




function getEvaluation($studid,$term = TERM){
    global $conn;
$query = "Select A.fldsubjectcode,A .flddescription,  B.fldlname ,A.fldfacid from (Select A.fldsubjectcode,C.flddescription,  B.fldfacid from tblsubjectstaken as A inner join tblsubjectoffered as B on A.fldsubjectcode = B.fldsubjectcode inner join tblsubjects as C on B.fldsubject = C.fldsubject where A.fldidnumber = ? and A.fldterm = ? and B.fldterm = ?) as A inner join tblfaculty as B on A.fldfacid = B.fldfacid";
    
if($stmt = $conn->prepare($query)){
        $stmt->bind_param("sss", $studid,$term,$term);
        $stmt->execute();
        $stmt->bind_result($a,$b,$c,$d);
        $wat = new evaluation();
        $stmt->store_result();
        while($stmt->fetch()){
            $stats = checkeval($studid,$a);
            $wat->add_class($a,utf8_encode($b),utf8_encode($c),$d,$stats);
        }        
        return $wat;       
    }

}

function checkeval($studid,$subj,$term = TERM)
{
    global $conn;
    $query = "select idnumber from tblevaluation where idnumber = ? and term = ? and subjectcode  = ? ";
    if($stmt = $conn->prepare($query)){
        $stmt->bind_param("sss", $studid,$term,$subj);
        $stmt->execute();
        $stmt->bind_result($a);
        $stmt->store_result();
        while($stmt->fetch()){
            return "true";
        }        
}
        return "false";   
    
}




?>