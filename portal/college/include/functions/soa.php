<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/functions/globalfunctions.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/portal/college/include/classes/soa.php');



function getInfo($key,$term = TERM){
    global $conn;
    $query = "Select fldprevaccount, fldtuitionfee, fldlabfee, fldschoolfee, (fldotherfee + fldadditionalfee) as otherfr, flddiscount, ((fldtuitionfee + fldlabfee + fldschoolfee + (fldotherfee + fldadditionalfee))) as 'totalcharge' FROM tblstudentenrolled where fldidnumber = ? and fldterm = ? limit 1;";
    if($stmta = $conn->prepare($query)){
        $stmta->bind_param("ss", $key,$term);
        $stmta->execute();
        $stmta->bind_result($prevaccount,$tutfee,$labfee,$schoolfee,$otherfee,$discount,$balance);
        
        while($stmta->fetch()){
            $subjects["account"] = array($prevaccount,$tutfee,$labfee,$schoolfee,$otherfee,$discount,$balance);
            
        }              
    }
     $query = "Select A.fldsubject, B.fldassessedamount, B.fldlabfee from (select B.fldsubject from tblsubjectstaken as A inner join tblsubjectoffered as B on A.fldsubjectcode = B.fldsubjectcode where A.fldidnumber = ? and A.fldterm = ? and B.fldterm = ?) as A inner join tblpricing as B on A.fldsubject = B.fldsubject where B.fldterm = ?;"; 
        
        if($stmtb = $conn->prepare($query)){
            $stmtb->bind_param("ssss",$key,$term,$term,$term);
            $stmtb->execute();
            $stmtb->bind_result($a,$b,$c);
            $subjects["subjects"] = array();
            while($stmtb->fetch()){
                    $subjects["subjects"][utf8_encode($a)] = array($b,$c);
                    //array_push($subjects["subjects"],$subjects[$a]);
                    //unset($subjects[$a]);
                }
            
            
        }
    
    $query = "SELECT fldremarks,fldamount  FROM tblstudledger WHERE fldidnumber = ? AND fldterm = ? and fldreference = ?;";
        
    if($stmtc = $conn->prepare($query)){
            $dis = "DISCOUNT";
            $stmtc->bind_param("sss",$key,$term,$dis);
            $stmtc->execute();
            $stmtc->bind_result($a,$b);
            $subjects["discount"] = array();
            while($stmtc->fetch()){    
                $subjects["discount"][utf8_encode($a)] = $b;
            }
                      
        }
    
    $query = "SELECT fldremarks,fldamount  FROM tblstudledger WHERE fldidnumber = ? AND fldterm = ? and fldreference = ?;";
        
    if($stmtc = $conn->prepare($query)){
            $dis = "ADD_CHARGE";
            $stmtc->bind_param("sss",$key,$term,$dis);
            $stmtc->execute();
            $stmtc->bind_result($a,$b);
            $subjects["adss"] = array();
            while($stmtc->fetch()){    
                $subjects["adss"][utf8_encode($a)] = $b;
            }
                      
        }
    
     $query = "SELECT fldpaymentdate,fldamount FROM tbltransactions WHERE fldidnumber = ? AND fldacctcode like ? AND fldterm = ?";
        
    if($stmtd = $conn->prepare($query)){
            $dis = "6010%";
            $stmtd->bind_param("sss",$key,$dis,$term);
            $stmtd->execute();
            $stmtd->bind_result($a,$b);
            $subjects["payments"] = array();
            while($stmtd->fetch()){    
                array_push($subjects["payments"],array($a,$b));
            }
            
                     
        }
    return json_encode($subjects); 

}


//getInfo("11212657","20162");

?>