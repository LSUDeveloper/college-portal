<?php

class Student{
private $info = array();   
    function __construct($term,$idnum,$name,$strand,$img,$dob,$gender,$a,$b,$c,$d,$e,$f){
        $this->info["hashed"] = sha1("random".$idnum."123");
        $this->info["term"] = $term;
        $this->info["idnum"] = $idnum;
        $this->info["name"] = $name;
        $this->info["course"] = $strand;
        $this->info["img"] = base64_encode($img);
        $this->info["dob"] = ($dob);
        $this->info["gender"] = ($gender);
        $this->info["card"] = ($a);
        $this->info["hondismissal"] = ($b);
        $this->info["goodmoral"] = ($c);
        $this->info["nso"] = ($d);
        $this->info["f137a"] = ($e);
        $this->info["tor"] = ($f);
        
    }

    
    function toJSON(){
        return json_encode($this->info);
    }
}

?>