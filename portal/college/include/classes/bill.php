<?php


class Bill {


    private $period,$previous_account,$tuition_fee,$lab_fee,$school_fee,$other_fee,$addi_charges,$addi_charges_one_time,$total_charges,$payments,$discount,$amount_due;


    function __construct($period,$previous_account,$tuition_fee,$lab_fee,$school_fee,$other_fee,$addi_charges,$addi_charges_one_time,$payments,$discount,$amount_due)
    {
        switch($period){
            case 1:
                $this->period = "PRELIM";
                break;
            case 2:
                $this->period = "MIDTERM";
                break;
            case 3:
                $this->period = "SEMI-FINAL";
                break;
            case 4:
                $this->period = "FINAL";
        }

        $this->previous_account = $previous_account;
        $this->tuition_fee = $tuition_fee;
        $this->lab_fee = $lab_fee;
        $this->school_fee = $school_fee;
        $this->other_fee = $other_fee;
        $this->addi_charges = $addi_charges;
        $this->addi_charges_one_time = $addi_charges_one_time;
        $this->total_charges =  $previous_account+$tuition_fee+$lab_fee+$school_fee+$other_fee+$addi_charges+$addi_charges_one_time;
        $this->payments = $payments;
        $this->discount = $discount;
        $this->amount_due = $amount_due;
    }

    function toJSON(){
        return '{"Period":"'.$this->period.
            '", "PrevAccount":"'.$this->previous_account.
            '", "TuitionFee":"'.$this->tuition_fee.
            '", "LabFee":"'.$this->lab_fee.
            '", "SchoolFee":"'.$this->school_fee.
            '", "OtherFee":"'.$this->other_fee.
            '", "AddiFee":"'.$this->addi_charges.
            '", "AddiFeeOneTime":"'.$this->addi_charges_one_time.
            '", "TotalCharges":"'.$this->total_charges.
            '", "Payments":"'.$this->payments.
            '", "Discount":"'.$this->discount.
            '", "AmountDue":"'.$this->amount_due.
            '"}';
    }




}













?>