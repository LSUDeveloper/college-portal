<?php

class clearance{

    private $clearance = array();
    private $countc  = 0 ;

    function __construct($term,$period){
        $this->clearance =  array("Term" => $term, "Period" =>$period );
    }

    public function add_clearance($office,$remarks,$status){
        $nam = "S".$this->countc;
        if(!isset($this->clearance["clearance"]))
            $this->clearance["clearance"] = array();        
        $this->clearance["clearance"][$nam] = array("office" => $office,"remarks" => $remarks, "status" => $status);
        $this->countc++;
    }

    public function toJSON(){
        return json_encode($this->clearance);
    }


}

?>